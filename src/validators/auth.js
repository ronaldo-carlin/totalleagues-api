const { check } = require("express-validator");
const validateResults = require("../utils/handleValidators");
const User = require("../models/auth");

const validatorLogin = [
  check("user").exists().notEmpty(),
  check("password").exists().notEmpty().isLength({ min: 6, max: 20 }),
  (req, res, next) => {
    return validateResults(req, res, next);
  },
];

const validatorCreateUser = [
  check("first_name").exists().notEmpty().isLength({ min: 1, max: 100 }),
  check("last_name").exists().isLength({ min: 1, max: 100 }),
  check("city").exists(),
  check("state").exists(),
  check("phone_number").exists().notEmpty().isLength({ min: 6, max: 20 }),
  //check("nickname").exists().notEmpty().isLength({ min: 1, max: 100 }),
  check("email")
    .exists()
    .notEmpty()
    .custom(async (value) => {
      // Verificar si el correo ya existe en la base de datos
      const user = await User.findOne({
        where: { email: value },
      });
      if (user) {
        throw new Error("El correo ya está en uso");
      }
    }),
  check("password").exists().notEmpty().isLength({ min: 6, max: 100 }),
  check("role_id").exists(),
  //check("image").exists(),
  //check("modules").exists(),
  // check("status").exists(),
  //check("created_at").exists(),
  (req, res, next) => {
    return validateResults(req, res, next);
  },
];

const validatorUpdateUser = [
  check("first_name").exists().notEmpty().isLength({ min: 1, max: 100 }),
  check("last_name").exists().isLength({ min: 1, max: 100 }),
  check("city").exists(),
  check("state").exists(),
  check("phone_number").exists().notEmpty().isLength({ min: 6, max: 20 }),
  check("nickname").exists().notEmpty().isLength({ min: 1, max: 100 }),
  check("email").exists().notEmpty().isLength({ min: 1, max: 100 }),
  check("password").exists(),
  check("role_id").exists(),

  (req, res, next) => {
    return validateResults(req, res, next);
  },
];

module.exports = { validatorLogin, validatorCreateUser, validatorUpdateUser };
