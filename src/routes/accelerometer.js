const express = require("express");

const router = express.Router();

const {
  getAccelerometer,
  createAccelerometer,
  getAccelerometerStats,
  getRecentAccelerometerStats,
} = require("../controllers/accelerometer");

router.get("/getAccelerometer", getAccelerometer);
router.post("/createAccelerometer", createAccelerometer);
router.get("/getAccelerometerStats", getAccelerometerStats);
router.get("/getRecentAccelerometerStats", getRecentAccelerometerStats);

module.exports = router;
