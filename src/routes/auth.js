const express = require("express");

const router = express.Router();

const uploadMiddleware = require("../utils/handleStorage");

const authMiddleware = require("../middleware/session");

const { validatorLogin, validatorCreateUser, validatorUpdateUser } = require("../validators/auth");

const { createUser, loginUser, getUser, getUsers, updateUser, resetPassword, uploadProfilePicture, deactivateUser, activateUser, deleteUser, checkToken, encryptPasswordsReset } = require("../controllers/auth");

/**
 * Route create user
 * @openapi
 * /auth/createUser:
 *   post:
 *      tags:
 *        - auth
 *      summary: "Se registra un usuario"
 *      description: "Esta ruta es para registra un usuario"
 *      security:
 *        - ApiKeyAuth: []
 *        - bearerAuth: []
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/userRegistration"
 *      responses:
 *          '201':
 *              description: "Usuario creado exitosamente"
 *          '403':
 *              description: "Error creando al usuario"
 */
router.post("/createUser", validatorCreateUser, createUser);
/**
 * Route login user
 * @openapi
 * /auth/loginUser:
 *   post:
 *      tags:
 *        - auth
 *      summary: "Login de usuario"
 *      description: "Esta ruta es para generar el login del usuario"
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/authLogin"
 *      responses:
 *          '201':
 *              description: "Bienvenido!"
 *          '403':
 *              description: "Datos incorrectos"
 */
router.post("/loginUser", validatorLogin, loginUser);
/**
 * Route reset password
 * @openapi
 * /auth/resetPassword:
 *   post:
 *      tags:
 *        - auth
 *      summary: "Recuperación de contraseña del usuario"
 *      description: "Esta ruta es para poder recuperar la contraseña del usuario"
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/resetPassword"
 *      responses:
 *          '201':
 *              description: "Mensaje de envío de correo de recuperación"
 *          '403':
 *              description: "Mensaje de envío de correo de recuperación"
 */
router.post("/resetPassword", resetPassword)
/**
 * Route getting all users
 * @openapi
 * /auth/getUsers:
 *   post:
 *      tags:
 *        - auth
 *      summary: "Obtener el listado de usuarios"
 *      description: "Esta ruta es para obtener todos los usuarios registrados con un status 1"
 *      security:
 *        - ApiKeyAuth: []
 *        - bearerAuth: []
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/getUsers"
 *      responses:
 *          '201':
 *              description: "Datos encontrados"
 *          '403':
 *              description: "Error consiguiendo datos"
 */
router.post("/getUsers", authMiddleware, getUsers);
/**
 * Route deactivating user
 * @openapi
 * /auth/deactivateUser/id:
 *   post:
 *      tags:
 *        - auth
 *      summary: "Se desactiva un usuario"
 *      description: "Esta ruta es para desactivar un usuario"
 *      security:
 *        - ApiKeyAuth: []
 *        - bearerAuth: []
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/deactivateUser"
 *      responses:
 *          '201':
 *              description: "Usuario deshabilitado"
 *          '403':
 *              description: "Error deshabilitando usuario"
 */
router.post("/deactivateUser/:id", authMiddleware, deactivateUser);
/**
 * Route activating user
 * @openapi
 * /auth/activateUser/id:
 *   post:
 *      tags:
 *        - auth
 *      summary: "Se activa un usuario"
 *      description: "Esta ruta es para activar un usuario"
 *      security:
 *        - ApiKeyAuth: []
 *        - bearerAuth: []
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/activateUser"
 *      responses:
 *          '201':
 *              description: "Usuario habilitado"
 *          '403':
 *              description: "Error habilitando usuario"
 */
router.post("/activateUser/:id", authMiddleware, activateUser);
/**
 * Route getting user
 * @openapi
 * /auth/getUser/id:
 *   post:
 *      tags:
 *        - auth
 *      summary: "Se obtiene un usuario en especifico"
 *      description: "Esta ruta es para obtener un usuario en especifico"
 *      security:
 *        - ApiKeyAuth: []
 *        - bearerAuth: []
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/getUser"
 *      responses:
 *          '201':
 *              description: "Usuario encontrado"
 *          '403':
 *              description: "Error encontrando usuario"
 */
router.post("/getUser/", getUser);
/**
 * Route updating user
 * @openapi
 * /auth/updateUser/id:
 *   post:
 *      tags:
 *        - auth
 *      summary: "Se actualiza un usuario en especifico"
 *      description: "Esta ruta es para actualizar un usuario en especifico"
 *      security:
 *        - ApiKeyAuth: []
 *        - bearerAuth: []
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/updatingUser"
 *      responses:
 *          '201':
 *              description: "Usuario actualizado"
 *          '403':
 *              description: "Error actualizando usuario"
 */
router.post("/updateUser/:id", authMiddleware,validatorUpdateUser, updateUser);
/**
 * Route deleting user
 * @openapi
 * /auth/deleteUser/id:
 *   post:
 *      tags:
 *        - auth
 *      summary: "Se elimina un usuario en especifico"
 *      description: "Esta ruta es para eliminar un usuario en especifico"
 *      security:
 *        - ApiKeyAuth: []
 *        - bearerAuth: []
 *      requestBody:
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: "#/components/schemas/deleteUser"
 *      responses:
 *          '201':
 *              description: "Usuario eliminado"
 *          '403':
 *              description: "Error eliminando usuario"
 */
router.post("/deleteUser/:id", authMiddleware, deleteUser);

router.post("/checkToken", checkToken)

router.post("/uploadImage", uploadMiddleware.single("myfile"), uploadProfilePicture);

router.post("/encryptPasswordsReset", authMiddleware, encryptPasswordsReset);

module.exports = router;