const express = require("express");

const router = express.Router();

const authMiddleware = require("../middleware/session");

const customHeader = require("../middleware/customHeader");

const { validatorCreateProcedures, validatorUpdateProcedures, validatorDeleteProcedures } = require("../validators/procedures");

const { createProcedure, getProcedures, updateProcedure, deleteProcedure } = require("../controllers/procedures");

router.post("/createProcedure", authMiddleware, validatorCreateProcedures, createProcedure);

router.post("/getProcedures", authMiddleware, getProcedures);

router.post("/updateProcedure", authMiddleware, validatorUpdateProcedures, updateProcedure);

router.post("/deleteProcedure", authMiddleware, validatorDeleteProcedures, deleteProcedure);

module.exports = router;