const express = require("express");

const router = express.Router();

const { getMexicoStates } = require("../controllers/mexico_states");

router.get("/getMexicoStates", getMexicoStates);

module.exports = router;
