const { dbConnectPostgresSql, sequelize } = require("../database/postgres");
const AuthModel = require("../models/auth");

const { handleHttpError } = require("../utils/handleError");

const { verifyToken } = require("../utils/handleJwt")

const authMiddleware = async (req, res, next) => {

    try {
        //Si no hay un Authorization en los headers retornamos error
        if (!req.headers.authorization) {
            handleHttpError(res, "NEED_SESSION", 401);
            return
        }
        //Si existe un Authorization en los headers quitamos todo lo que este antes del espacio
        const token = req.headers.authorization.split(' ').pop();
        //Verificamos el token
        const dataToken = await verifyToken(token);
        console.log(dataToken)
        //Si no hay informacion en el token retornamos error
        if (!dataToken) {
            handleHttpError(res, "NOT_PAYLOAD_DATA", 401);
            return
        }
        //Obtenemos el id que viene en el token para consultar en la base de datos
        const query = {
            Id: dataToken.id
        }
        //Buscamos los datos del usuario en la base de datos en base al id del token
        const user = await AuthModel.findOne({
            query,
            attributes: { exclude: ['password'] }
        })
        //Enviamos los datos
        req.user = await user;
        next()
    } catch (e) {
        console.log('session',e)
        handleHttpError(res, "NOT_SESSION", 401);
    }
};

module.exports = authMiddleware;