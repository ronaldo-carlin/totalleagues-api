const { handleHttpError } = require("../utils/handleError");

const dynamics = (req, res, next) => {
    try {
        const origin = req.headers.origin;
        if(origin === "https://cnciedu.crm.dynamics.com"){
            next();
        } else {
            handleHttpError(res, 'ORIGEN_DESCONOCIDO.', 403)
        }
    } catch (error) {
        handleHttpError(res, 'PROBLEMAS_EN_LOS_CUSTOM_HEADER.', 403)
    }
}

module.exports = dynamics;