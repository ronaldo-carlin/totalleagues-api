// index.js
const app = require('./app');
const { io, server } = require('./socket'); // Importa 'io' y 'server'
const config = require('./config/config');

const PORT = config.PORT || 3011;
const SOCKET_PORT = config.SOCKET_PORT || 3012;

async function main() {
  await app.listen(PORT);
  console.log("<<< Server is running on port http://localhost:" + PORT + ">>>");

  server.listen(SOCKET_PORT, () => {
    console.log("<<< Socket.io Server is running on port http://localhost:" + SOCKET_PORT + ">>>");
  });
}

main();