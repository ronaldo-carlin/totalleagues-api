const dotenv = require('dotenv');
const path = require('path');

dotenv.config({
	path: path.resolve(__dirname, `${process.env.NODE_ENV}.env`)
});

module.exports = {
	NODE_ENV: process.env.NODE_ENV,
	PORT: process.env.PORT,
	PUBLIC_URL: process.env.PUBLIC_URL,
	PUBLIC_FRONT_URL: process.env.PUBLIC_FRONT_URL,

	JWT_SECRET: process.env.JWT_SECRET,
	
	POSTGRES_DATABASE: process.env.POSTGRES_DATABASE,
	POSTGRES_USER: process.env.POSTGRES_USER,
	POSTGRES_PASSWORD: process.env.POSTGRES_PASSWORD,
	POSTGRES_HOST: process.env.POSTGRES_HOST,

	PATH_SOCKET: process.env.PATH_SOCKET,
	SOCKET_PORT: process.env.SOCKET_PORT,

	APIKEY: process.env.APIKEY,

	MAIL_HOST: process.env.MAIL_HOST,
	MAIL_PORT: process.env.MAIL_PORT,
	MAIL_USER: process.env.MAIL_USER,
	MAIL_PASSWORD: process.env.MAIL_PASSWORD,
}