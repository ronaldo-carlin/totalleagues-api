const { Sequelize } = require("sequelize");
const { sequelize } = require("../database/postgres");

const MexicoStatesModules = sequelize.define(
  "mexico_states",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    key: {
      type: Sequelize.TEXT,
    },
    name: {
      type: Sequelize.TEXT,
    },
    number: {
      type: Sequelize.TEXT,
    },
    town: {
      type: Sequelize.TEXT,
    },
  },
  {
    timestamps: false, // Esto habilita automáticamente createdAt y updatedAt
  }
);

module.exports = MexicoStatesModules;
