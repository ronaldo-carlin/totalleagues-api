const { Sequelize } = require("sequelize");
const { sequelize } = require("../database/postgres");

const AccelerometerModules = sequelize.define(
  "accelerometer",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    x: {
      type: Sequelize.INTEGER,
    },
    y: {
      type: Sequelize.INTEGER,
    },
    z: {
      type: Sequelize.INTEGER,
    }
  },
  {
    timestamps: false, // Esto habilita automáticamente createdAt y updatedAt
    freezeTableName: true // Esto evita que sequelize pluralice el nombre de la tabla
  }
);

module.exports = AccelerometerModules;
