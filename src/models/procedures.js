const { Sequelize } = require('sequelize');
const { sequelize } = require('../database/postgres');

const ProceduresModules = sequelize.define('Procedimientos', {
    Id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    Guid: {
        type: Sequelize.TEXT,
    },
    Nombre: {
        type: Sequelize.TEXT,
    },
    Estatus: {
        type: Sequelize.INTEGER,
    },
    CreadoPor: {
        type: Sequelize.TEXT,
    },
    CreadoEn: {
        type: Sequelize.DATE,
    },
    ActualizadoPor: {
        type: Sequelize.TEXT,
    },
    ActualizadoEn: {
        type: Sequelize.DATE,
    },
    EliminadoPor: {
        type: Sequelize.TEXT,
    },
    EliminadoEn: {
        type: Sequelize.DATE,
    },
},
    {
        createdAt: false,
        updatedAt: false,
        schema: "MET"
    });

module.exports = ProceduresModules;