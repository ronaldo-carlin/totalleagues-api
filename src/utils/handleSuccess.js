const { response } = require("express");
const { sequelize } = require("../database/postgres");

const handleHttpSuccess = (res, success, msg = 'Todo Ok!', code = 201, data) => {
    res.status(code);

    const responseObj = {
        success: success,
        message: msg,
        data: data
    };

    if (Array.isArray(data)) {
        responseObj.count = data.length;  // Agrega el contador si los datos son un array
    }

    res.send(responseObj);
};

module.exports = { handleHttpSuccess };
