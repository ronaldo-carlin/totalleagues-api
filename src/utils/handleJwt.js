const jwt = require("jsonwebtoken");

const JWT_SECRET = process.env.JWT_SECRET;

/**
 * Objeto de usuario
 * @param {*} user
 */
const tokenSign = async (user) => {
  const sign = jwt.sign(
    {
      id: user.id,
      role: user.rol_id,
    },
    JWT_SECRET,
    {
      expiresIn: "24h",
    }
  );

  return sign;
};

const resetPasswordToken = async (user) => {
  const resetPassword = jwt.sign(
    {
      userId: user.id,
      resetPassword: true,
    },
    JWT_SECRET,
    {
      expiresIn: "30m", // Token de restablecimiento de contraseña válido por 1 hora
    }
  );
  return resetPassword;
};

/**
 * Pasar token de sesion
 * @param {*} tokenJwt
 * @returns
 */
const verifyToken = async (tokenJwt) => {
  try {
    return jwt.verify(tokenJwt, JWT_SECRET);
  } catch (err) {
    return null;
  }
};

module.exports = { tokenSign, verifyToken, resetPasswordToken };
