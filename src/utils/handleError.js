const { response } = require("express");
const { sequelize } = require("../database/postgres");

const handleHttpError = (res, msg = 'Algo ha ocurrido, intentelo nuevamente', code = 403) => {

    res.status(code);

    res.send({
        error: msg
    })

}

module.exports = { handleHttpError }