// socket.js
const express = require("express");
const app_socket = express();
const http = require("http");
const server = http.createServer(app_socket);
const { Server } = require("socket.io");
/* const config = require("./config/config"); */
const sesionSocketMiddleware = require("./middleware/session_socket");
const { Socket } = require("dgram");
//const sc = require("./controllers/socket_notifications.controller");
const AccelerometerModules = require("./models/accelerometer");

const io = new Server(server, {
  cors: {
    origins: ["*"],
  },
});

app_socket.use(express.json());

app_socket.use(function (req, res, next) {
  res.io = io;
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

//require("./routes/socket_notifications")(app_socket);
//require("./routes/main")(app_socket);

//io.use(sesionSocketMiddleware);

io.on("connection", (client) => {
  console.log("<<< Nuevo cliente conectado: " + client.id + " >>>");

  const emitacelerometro = async () => {
    // Obtén todos los registros de la tabla en orden descendente
    const alecerometro = await AccelerometerModules.findAll({
      order: [["id", "DESC"]],
    });
    client.emit("socket:accelerometerData", alecerometro);
    //console.log("<<< Datos del acelerómetro recibidos >>>", alecerometro);
  };
  emitacelerometro();

  client.on("client:accelerometerData", (data) => {
    console.log("Datos del acelerómetro recibidos en el servidor:", data);
    // Cuando se recibe un evento 'client:accelerometerData' desde el ESP32, se envía directamente a todos los clientes conectados.
    client.broadcast.emit("socket:accelerometerData", data);
  });

  client.on("message", function (data) {
    console.log("Mensaje recibido: ", data);
    client.emit("message", data);
  });

  client.on("client:ping", () => {
    client.emit("pong");
  });

  client.on("disconnect", (reason) => {
    console.log("<<< Cliente desconectado >>>");
  });
});

module.exports = {
  io,
  server, // Asegúrate de exportar 'server' junto con 'io'
};
