const AccelerometerModules = require("../models/accelerometer");

const { handleHttpError } = require("../utils/handleError");

const { handleHttpSuccess } = require("../utils/handleSuccess");

const { io } = require("../socket");

const { sendEmail } = require("../helpers/mailer");

/* endpoint para crear los datos del acelerometro */
const createAccelerometer = async (req, res) => {
  try {
    // Obtén los datos desde el body
    const { x, y } = req.body;

    //console.log("Datos del acelerometro: ", req.body);

    // Aplicar el filtro de suavizado exponencial a la coordenada x
    const alpha = 0.2;
    let sX = 0;
    sX = alpha * x + (1 - alpha) * sX;

    // Aplicar el filtro de suavizado exponencial a la coordenada y
    let sY = 0;
    sY = alpha * y + (1 - alpha) * sY;

    // Crea un nuevo registro en la tabla
    const newAccelerometer = await AccelerometerModules.create({
      x,
      y,
    });

    const serializedAccelerometerData = newAccelerometer.toJSON(); // Convierte el objeto a un formato JSON serializable

    serializedAccelerometerData.sx = sX; // Agrega la coordenada x suavizada al objeto
    serializedAccelerometerData.sy = sY; // Agrega la coordenada y suavizada al objeto

    console.log(
      "Emitiendo datos del acelerometro:",
      serializedAccelerometerData
    );

    // Emitir todos los registros a través de sockets
    io.emit("socket:accelerometerData", serializedAccelerometerData);

    // Envía el correo de advertencia, para decir que el acelerometro se ha pasado de los limites
    /* if (x > 1.5 || y > 1.5) {
      await sendEmail({
        to: "roiksg9@gmail.com",
        subject: "Advertencia de acelerometro",
        html: `<p>El acelerometro se ha pasado de los limites.<br> ${JSON.stringify(
          serializedAccelerometerData
        )} </p>`,
      });
    } */

    // Envía el registro al front-end
    handleHttpSuccess(
      res,
      true,
      "Se ha guardado el dato del acelerometro.",
      200,
      newAccelerometer
    );
  } catch (error) {
    console.log("Error: ", error);
    // Devuelve el error al front-end
    handleHttpError(res, "Error guardando el dato del acelerometro.", 500);
  }
};

/* endpoint para obtener todos los datos del acelerometro */
const getAccelerometer = async (req, res) => {
  try {
    // Obtén todos los registros de la tabla
    const accelerometer = await AccelerometerModules.findAll();

    // Envía los registros al front-end
    handleHttpSuccess(
      res,
      true,
      "Se han encontrado los datos del acelerometro.",
      200,
      accelerometer
    );
  } catch (error) {
    console.log("Error: ", error);
    // Devuelve el error al front-end
    handleHttpError(res, "Error obteniendo datos del acelerometro.", 500);
  }
};

const getAccelerometerStats = async (req, res) => {
  try {
    const { coordinate } = req.query;

    // Validar que el parámetro "coordinate" sea proporcionado y sea válido ("x" o "y")
    if (!coordinate || (coordinate !== "x" && coordinate !== "y")) {
      handleHttpError(
        res,
        "Parámetro 'coordinate' inválido. Debe ser 'x' o 'y'.",
        400
      );
      return;
    }

    // Obtener los últimos 250 registros de la tabla en orden descendente
    const accelerometer = await AccelerometerModules.findAll({
      order: [["id", "DESC"]],
      limit: 250,
    });

    // Verificar si hay suficientes registros
    if (accelerometer.length >= 250) {
      const result = [];

      // Iterar en conjuntos de 25 registros para calcular los valores
      for (let i = 0; i < 10; i++) {
        const startIndex = i * 25;
        const endIndex = startIndex + 25;
        const subset = accelerometer.slice(startIndex, endIndex);

        // Calcular el punto más alto, el punto más bajo y la media para la coordenada especificada
        const max = Math.max(...subset.map((record) => record[coordinate]));
        const min = Math.min(...subset.map((record) => record[coordinate]));
        //const avg = subset.reduce((sum, record) => sum + record[coordinate], 0) / 25; // calcula el promedio de los 25 registros
        const avg = (max + min) / 2; //calcula la media de los 25 registros

        // Agregar resultados al array de resultados
        result.push({
          max,
          min,
          avg,
        });
      }

      // Devolver los resultados al cliente
      handleHttpSuccess(
        res,
        true,
        `Estadísticas de acelerómetro (${coordinate}) obtenidas.`,
        200,
        result
      );
    } else {
      // No hay suficientes registros
      handleHttpError(
        res,
        "No hay suficientes registros para calcular las estadísticas.",
        400
      );
    }
  } catch (error) {
    console.log("Error: ", error);
    // Devuelve el error al front-end
    handleHttpError(res, "Error obteniendo estadísticas de acelerómetro.", 500);
  }
};

const getRecentAccelerometerStats = async (req, res) => {
  try {
    const { coordinate } = req.query;

    // Validar que el parámetro "coordinate" sea proporcionado y sea válido ("x" o "y")
    if (!coordinate || (coordinate !== "x" && coordinate !== "y")) {
      handleHttpError(
        res,
        "Parámetro 'coordinate' inválido. Debe ser 'x' o 'y'.",
        400
      );
      return;
    }

    // Obtener los últimos 500 registros de la tabla en orden descendente
    const accelerometer = await AccelerometerModules.findAll({
      order: [["id", "DESC"]],
      limit: 500,
    });

    // Verificar si hay suficientes registros
    if (accelerometer.length >= 500) {
      const result = [];

      // Calcular el punto más alto, el punto más bajo y la media para la coordenada especificada
      const max = Math.max(
        ...accelerometer.map((record) => record[coordinate])
      );
      const min = Math.min(
        ...accelerometer.map((record) => record[coordinate])
      );
      /* const avg =
        accelerometer.reduce((sum, record) => sum + record[coordinate], 0) /
        500; */
        const avg = (max + min) / 2; //calcula la media de los 500 registros

      // Devolver los resultados al cliente
      handleHttpSuccess(
        res,
        true,
        `Estadísticas de acelerómetro (${coordinate}) obtenidas.`,
        200,
        {
          max,
          min,
          avg,
        }
      );
    } else {
      // No hay suficientes registros
      handleHttpError(
        res,
        "No hay suficientes registros para calcular las estadísticas.",
        400
      );
    }
  } catch (error) {
    console.log("Error: ", error);
    // Devuelve el error al front-end
    handleHttpError(res, "Error obteniendo estadísticas de acelerómetro.", 500);
  }
};

module.exports = {
  getAccelerometer,
  createAccelerometer,
  getAccelerometerStats,
  getRecentAccelerometerStats,
};
