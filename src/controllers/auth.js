const { response } = require("express");

// const { dbConnectPostgresSql, sequelize } = require("../database/postgres");

const { Sequelize, Op } = require("sequelize");

const { encrypt, compare } = require("../utils/handlePassword");

const { matchedData } = require("express-validator");

const { v4: uuidv4 } = require("uuid");

const AuthModel = require("../models/auth");

const { tokenSign } = require("../utils/handleJwt");

const { handleHttpError } = require("../utils/handleError");

const fs = require("fs");

const ejs = require("ejs");

const PUBLIC_URL = process.env.PUBLIC_URL + "/users";

const EMAIL_TEMPLATE = __dirname + "/../html/resetPsw.html";

const { verifyToken } = require("../utils/handleJwt");

const { handleHttpSuccess } = require("../utils/handleSuccess");

const moment = require("moment");

const { sendEmail } = require("../helpers/mailer");

/**
 * Este controlador es el encargado de registrar un usuario
 * @param {*} req
 * @param {*} res
 */
const createUser = async (req, res) => {
  try {
    //Obtenemos el body del request
    const data = matchedData(req);
    //Creamos el guid
    const guid = await uuidv4();
    //Ciframos la contraseña
    const password = await encrypt(data.password);
    // seteamos la fecha de hoy en el registro
    const created_at = moment().toISOString();
    //Asignamos la contraseña cifrada, el guid y el  Rol al objeto body
    let body = { ...data, password, guid, created_at };
    // constante que almacena el arreglo de los modulos
    const Modulos = data.Modulos ? data.Modulos : "";
    // creado por
    const CreadoPor = data.CreadoPor;
    // Creamos el usuario
    const dataUser = await AuthModel.create(body)
      .then((usuario) => {
        const IdUsuario = usuario.id;
        const role_id = usuario.role_id;
        body = { ...data, password, guid, created_at, id: usuario.id };
        console.log("usuarioId", IdUsuario, "role_id", role_id);

        handleHttpSuccess(res, true, "Usuario creado exitosamente.", 201, [
          body,
        ]);
      })
      .catch((error) => {
        console.log(error);
        handleHttpError(res, "Error al crear el usuario.", 403);
      });
  } catch (error) {
    //Obtener y devolver el error
    handleHttpError(res, "Error al crear el usuario.", 403);
  }
};

/**
 * Este controlador es el encargado de realizar el login del usuario
 * @param {*} req
 * @param {*} res
 */
const loginUser = async (req, res) => {
  try {
    //Obtenemos y limpiamos el body del request
    const body = matchedData(req);
    //Buscamos el usuario en la base de datos
    const user = await AuthModel.findOne({
      where: {
        [Op.or]: [
          {
            nickname: body.user.toLowerCase(),
          },
          {
            email: body.user.toLowerCase(),
          },
          {
            phone_number: body.user.toLowerCase(),
          },
        ],
      },
    });
    //Si el usuario no existe enviamos una respuesta de error
    if (!user) {
      handleHttpError(res, "El usuario no existe.", 404);
      return;
    }
    //Si el usuario esta inactivo enviamos una respuesta de error
    if (user.active == false) {
      handleHttpError(res, "El usuario ha sido deshabilitado.", 401);
      return;
    }
    //Ciframos la contraseña para compararla con la de la base de datos
    const hashPassword = user.get("password");
    //Comparamos la contraseña
    /* if (body.password != hashPassword) {
            handleHttpError(res, 'Datos incorrectos.', 403)
            return;
        } */
    const check = await compare(body.password, hashPassword);
    //Si la contraseña es incorrecta enviamos una respuesta de error
    if (!check) {
      handleHttpSuccess(res, false, "Datos incorrectos.", 401, null);
      return;
    }
    //Si se logra logear se envía un objeto con el token y los datos del usuario
    const data = {
      token: await tokenSign(user),
      user,
    };
    //Si el usuario se puede logear actualizamos la ultima conexión
    const fechaActual = new Date();
    /* const opciones = { year: "numeric", month: "long", day: "numeric" };
    const fechaFormateada = fechaActual.toLocaleDateString("es-MX", opciones); */

    await AuthModel.update(
      { last_login: fechaActual },
      {
        where: {
          guid: user.guid,
        },
      }
    );
    //Enviamos una respuesta de éxito con los datos
    handleHttpSuccess(res, true, "¡Bienvenido!", 201, data);
    // Enviamos el correo de inicio de sesión
    await sendEmail({
      to: user.email,
      subject: "Inicio de sesión exitoso",
      html: `<p>Hola ${user.nickname}, se ha iniciado sesión en tu cuenta desde el siguiente dispositivo: ${req.headers["user-agent"]}</p>`,
    });
  } catch (error) {
    //Obtener y devolver el error
    console.log("error login:", error);

    handleHttpError(res, "Error login de usuario.", 403);
  }
};

/**
 * Este controlador es el encargado de recuperar la contraseña
 * @param {*} req
 * @param {*} res
 */
const resetPassword = async (req, res) => {
  try {
    //Obtenemos el body del request
    req = req.body;
    //Buscamos si el usuario existe
    const user = await AuthModel.findOne({
      where: {
        [Op.or]: [
          {
            email: req.user,
          },
        ],
      },
    });
    //En caso de que no se haya encontrado un usuario
    if (!user) {
      //Enviamos un mensaje de success falso
      handleHttpSuccess(
        res,
        true,
        "Si su usuario o E-Mail existen en la base de datos, se enviará un correo con el link de recuperación de contraseña.",
        201,
        null
      );
      return;
    }
    // Genera un token de restablecimiento de contraseña
    const data = {
      token: await tokenSign(user),
      user,
    };
    // Leemos el fichero de HTML que usaremos para el cuerpo del mensaje
    let html = fs.readFileSync(EMAIL_TEMPLATE, "utf-8");
    console.log("user: ", user.dataValues);
    const context = {
      name: `${user.first_name} ${user.last_name}`,
      front_url: `${process.env.PUBLIC_FRONT_URL}`,
      action_url: `${process.env.PUBLIC_FRONT_URL}/ResetPassword/` + data.token,
    };
    // Renderizar el HTML con los datos
    const contenidoHTML = ejs.render(html, context);

    // Creamos las opciones del correo
    let mailOptions = {
      from: '"PROMOFENIX"' + process.env.MAIL_USER + "", // sender address
      to: req.user,
      subject: "Solicitud de recuperación de contraseña",
      html: contenidoHTML,
    };

    // Configuramos el transported de nodemailer
    let transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      secure: false, // Cambiar a false para usar TLS en lugar de SSL
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD,
      },
    });

    // Enviar correo electrónico
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        handleHttpError(res, "Ha ocurrido un error enviando el correo.", 403);
        return console.log(error);
      }
      handleHttpSuccess(
        res,
        true,
        "Se enviará un correo con el link de recuperación de contraseña.",
        201,
        null
      );
    });
  } catch (error) {
    //Obtener y devolver el error
    console.log("error resetPassword", error);
    handleHttpError(res, "Ha ocurrido un error en el try.", 403);
  }
};

/**
 * Este controlador es el encargado de obtener el usuario
 * @param {*} req
 * @param {*} res
 */
const getUser = async (req, res) => {
  try {
    const body = req.body;

    console.log("body: ", body);

    let searchQuery = {};

    if (typeof body.user === "string") {
      if (body.user.includes("@")) {
        searchQuery = { email: body.user.toLowerCase() };
      } else {
        searchQuery = { phone_number: body.user };
      }
    } else if (typeof body.user === "number") {
      searchQuery = { id: body.user };
    } else {
      throw new Error(
        'El campo "user" debe ser una cadena de texto o un número entero.'
      );
    }
    console.log("searchQuery: ", searchQuery);
    // Buscamos el referenciado en la base de datos
    const user = await AuthModel.findOne({
      where: searchQuery,
      //Excluimos la contraseña
      attributes: { exclude: ["password"] },
    });

    //Si el usuario no existe enviamos una respuesta de error
    if (!user) {
      handleHttpError(res, "El usuario no existe.", 404);
      return;
    }
    //Si el usuario esta inactivo enviamos una respuesta de error
    if (user.active == false) {
      handleHttpError(res, "El usuario ha sido deshabilitado.", 401);
      return;
    }

    //Si existe el referenciado lo enviamos al front
    handleHttpSuccess(
      res,
      true,
      "Se ha encontrado los datos del usuario.",
      201,
      user
    );
  } catch (error) {
    //Obtener y devolver el error
    handleHttpError(res, "Error obteniendo usuario.", 403);
  }
};

/**
 * Este controlador es el encargado de obtener los usuarios
 * @param {*} req
 * @param {*} res
 */
const getUsers = async (req, res) => {
  try {
    //Buscamos todos los usuarios
    const user = await AuthModel.findAll({
      where: { status: "1" },
      attributes: {
        //Excluimos las contraseñas
        exclude: ["password"],
      },
    });
    //Si no existen usuarios enviamos error
    if (!user) {
      handleHttpError(res, "El usuario no existe.", 404);
      return;
    }
    const message = user.length
      ? "Se han encontrado los datos."
      : "No se encontraron datos.";
    handleHttpSuccess(res, true, message, 201, user);
  } catch (error) {
    //Obtener y devolver el error
    handleHttpError(res, error, 403);
  }
};

/**
 * Este controlador es el encargado de actualizar el usuario
 * @param {*} req
 * @param {*} res
 */
const updateUser = async (req, res) => {
  try {
    //Obtenemos el usuario enviado por el cuerpo
    const id = req.params.id;
    //Obtenemos el body del request que será actualizado
    const matchedBody = matchedData(req);
    //Creamos la variable que obtendrá la data del usuario actualizada
    let data;

    // constante que almacena el arreglo de los modulos
    //const Modulos = matchedBody.Modulos;
    //constante que almacena el usuario quien actualizo
    //const ActualizadoPor = matchedBody.ActualizadoPor;
    //Si el cuerpo enviado tiene una contraseña
    console.log(matchedBody);
    if (matchedBody.password != "") {
      findPassword = await AuthModel.findOne({
        where: { id: id },
      });

      // Comparamos la contraseña actual  con la contraseña registrada en db
      comparePassword = await compare(
        matchedBody.passwordActual,
        findPassword.password
      );
      // Si la comparacion es verdadera, son iguales
      if (comparePassword) {
        // Encriptamos la nueva contraseña
        const password = await encrypt(matchedBody.password);
        //Creamos el nuevo body con la contraseña ya encriptada
        body = { ...matchedBody, password };
        // Actualizamos la información
        data = AuthModel.update(
          {
            first_name: Body.first_name,
            last_name: Body.last_name,
            city: Body.city,
            state: Body.state,
            phone_number: Body.phone_number,
            nickname: Body.nickname,
            email: Body.email,
            role_id: Body.role_id,
            password: body.password,
          },
          {
            where: {
              id: id,
            },
            returning: true,
          }
        ) /* 
          .then(async ([rowsUpdated, [usuario]]) => {
            const IdUsuario = usuario.id;
            const role_id = usuario.role_id;
            // Llamar a la función updateRolesPermissionByUser después de actualizar el registro
            await updateRolesPermissionByUser(
              role_id,
              IdUsuario,
              Modulos,
              ActualizadoPor
            );
          }) */
          .then(() => {
            handleHttpSuccess(
              res,
              true,
              "Usuario actualizado exitosamente.",
              201,
              [data]
            );
          })
          .catch((error) => {
            handleHttpError(res, "Error al actualizar el usuario.", 403);
          });
      } else {
        //Error las contraseñas no coinciden
        return handleHttpError(
          res,
          "La contraseña actual no coincide con la contraseña registrada.",
          403
        );
      }
    } else {
      //Si no existe contraseña que actualizar actualizamos la información del usuario
      data = await AuthModel.update(
        {
          first_name: matchedBody.first_name,
          last_name: matchedBody.last_name,
          city: matchedBody.city,
          state: matchedBody.state,
          phone_number: matchedBody.phone_number,
          nickname: matchedBody.nickname,
          email: matchedBody.email,
          role_id: matchedBody.role_id,
        },
        {
          where: {
            id: id,
          },
          returning: true,
        }
      )
        .then(async ([rowsUpdated, [usuario]]) => {
          const IdUsuario = usuario.id;
          const role_id = usuario.role_id;
          // Llamar a la función updateRolesPermissionByUser después de actualizar el registro
          /* await updateRolesPermissionByUser(
            role_id,
            IdUsuario,
            Modulos,
            ActualizadoPor
          ); */
        })
        .then(() => {
          handleHttpSuccess(
            res,
            true,
            "Usuario actualizado exitosamente.",
            201,
            [data]
          );
        })
        .catch((error) => {
          handleHttpError(res, "Error al actualizar el usuario.", 403);
        });
    }
    //Enviamos los datos actualizados
    // handleHttpSuccess(res, true, 'Usuario actualizado.', 201, data)
  } catch (error) {
    //Obtener y devolver el error
    handleHttpError(res, "Error actualizando el usuario. " + error, 403);
  }
};

/**
 * Este controlador es el encargado de subir una imagen del usuario
 * @param {*} req
 * @param {*} res
 */
const uploadProfilePicture = async (req, res) => {
  try {
    const { file } = req;

    const fileData = {
      filename: file.filename,

      url: `${PUBLIC_URL}/${file.filename}`,
    };

    handleHttpSuccess(res, true, "Imagen subida.", 201, fileData);
  } catch (error) {
    //Obtener y devolver el error
    handleHttpError(res, "Error al guardar la imagen.", 403);
  }
};

/**
 * Este controlador es el encargado de desactivar el usuario
 * @param {*} req
 * @param {*} res
 */
const deactivateUser = async (req, res) => {
  try {
    const id = req.params.id;

    data = await AuthModel.update(
      { active: false },
      {
        where: {
          id: id,
        },
      }
    );

    handleHttpSuccess(res, true, "Usuario deshabilitado.", 201, data);
  } catch (error) {
    //Obtener y devolver el error
    handleHttpError(res, "Error deshabilitando el usuario.", 403);
  }
};

/**
 * Este controlador es el encargado de activar el usuario
 * @param {*} req
 * @param {*} res
 */
const activateUser = async (req, res) => {
  try {
    const id = req.params.id;

    data = await AuthModel.update(
      { active: true },
      {
        where: {
          id: id,
        },
      }
    );

    handleHttpSuccess(res, true, "Usuario habilidato.", 201, data);
  } catch (error) {
    //Obtener y devolver el error
    handleHttpError(res, "Error habilitando el usuario.", 403);
  }
};

/**
 * Este controlador es el encargado de eliminar el usuario
 * @param {*} req
 * @param {*} res
 */
const deleteUser = async (req, res) => {
  try {
    const id = req.params.id;

    data = await AuthModel.findOne({
      where: {
        id: id,
      },
    }).then((data) => {
      if (!data) {
        handleHttpError(res, "No se han encontrado datos.", 403);

        return;
      }

      // data.destroy()
      data.update({ status: "0" });

      handleHttpSuccess(res, true, "Usuario eliminado.", 201, data);
    });
  } catch (error) {
    //Obtener y devolver el error
    handleHttpError(res, "Error al eliminar el usuario.", 403);
  }
};

/**
 * Este controlador es el encargado de revisar si el token que usar el usuario es valido
 * @param {*} req
 * @param {*} res
 */
const checkToken = async (req, res) => {
  try {
    let token = req.headers.authorization?.split(" ").pop(); // Intenta obtener el token de la cabecera de autorización
    if (!token) {
      // Si no se encuentra en la cabecera de autorización, intenta obtenerlo de un parámetro de consulta llamado "token"
      console.log("req.body: ", req.body);
      token = req.body.token;
    }

    if (!token) {
      // Si no se proporciona un token en la cabecera de autorización ni en la consulta, responde con un error.
      return handleHttpError(res, "Token no proporcionado.", 403);
    }

    // Verifica el token
    const dataToken = await verifyToken(token);
    // Obtiene el id que viene en el token para consultar en la base de datos
    const query = {
      id: dataToken.id,
    };
    // Busca los datos del usuario en la base de datos en base al id del token
    const user = await AuthModel.findOne({
      where: query,
      attributes: { exclude: ["password"] },
    });
    // Envía los datos del usuario
    handleHttpSuccess(res, true, "Token válido", 201, user);
  } catch (error) {
    console.log(error);
    // Maneja los errores adecuadamente
    handleHttpError(res, "Error al verificar el token.", 403);
  }
};

// Endpoint para encriptar todas las contraseñas de la tabla de usuarios
const encryptPasswords = async (req, res) => {
  try {
    // Obtén todos los usuarios de la tabla
    const user = await UserModel.find({
      created_by: 1,
      created_at: "2023-07-05",
    });
    const users = await AuthModel.findAll({
      where: { created_by: 1, created_at: "2023-07-05" },
      attributes: {
        //Excluimos las contraseñas
        exclude: ["password"],
      },
    });

    // Encripta las contraseñas de cada usuario
    /* for (let i = 0; i < users.length; i++) {
      const user = users[i];
      const encryptedPassword = await encrypt(user.password);

      // Actualiza la contraseña en el usuario
      user.password = encryptedPassword;
      await user.save();
    } */
    const message = "Se encontraron datos.";
    handleHttpSuccess(res, true, message, 201, user);
  } catch (error) {
    console.log(error);
    handleHttpError(res, error, 403);
  }
};

// Endpoint para encriptar la contraseña al usuario
const encryptPasswordsReset = async (req, res) => {
  try {
    //Obtenemos el body del response y extraemos los campos necesarios
    const { user, new_password } = req.body;

    //Buscamos si el usuario existe
    const foundUser = await AuthModel.findOne({
      where: {
        [Op.or]: [
          {
            phone_number: user,
          },
          {
            email: user,
          },
        ],
      },
    });

    console.log("foundUser: ", foundUser);

    //En caso de que no se haya encontrado un usuario
    if (!foundUser) {
      //Enviamos un mensaje de error
      handleHttpError(res, "No se encontro el usuario.", 403);
      return;
    }

    if (new_password) {
      const encryptedPassword = await encrypt(new_password);
      // Actualiza la contraseña en el usuario
      foundUser.password = encryptedPassword;
      await foundUser.save();
      handleHttpSuccess(
        res,
        true,
        "Se ha actualizado la contraseña del usuario con éxito.",
        201,
        foundUser
      );
    } else {
      //Enviamos un mensaje de error
      handleHttpError(res, "Favor de poner la contraseña.", 403);
    }
  } catch (error) {
    //Obtener y devolver el error
    handleHttpError(res, "Ha ocurrido un error.", 403);
  }
};

module.exports = {
  createUser,
  loginUser,
  getUser,
  getUsers,
  updateUser,
  resetPassword,
  uploadProfilePicture,
  deactivateUser,
  activateUser,
  deleteUser,
  checkToken,
  encryptPasswords,
  encryptPasswordsReset,
};
