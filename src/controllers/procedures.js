const { response } = require("express");

const { Op } = require("sequelize");

const { matchedData } = require("express-validator");

const { v4: uuidv4 } = require('uuid');

const ProceduresModules = require("../models/procedures");

const { handleHttpError } = require("../utils/handleError");

const { handleHttpSuccess } = require("../utils/handleSuccess");

/**
 * Este controlador es el encargado de registrar un procedimiento
 * @param {*} req 
 * @param {*} res 
 */
const createProcedure = async (req, res) => {
    try {
        //Obtenemos el body request
        req = req.body
        //Creamos el UUID para el registro
        const Guid = await uuidv4();
        //Construimos el nuevo body a guardar
        const body = { ...req, Guid };
        //Creamos el instrumento
        const data = await ProceduresModules.create(body);
        //Obtener data y enviar la respuesta
        handleHttpSuccess(res, true, 'Procedimiento creado exitosamente.', 201, data)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, 'Error creando procedimiento.', 403)
    }
}
/**
 * Este controlador es el encargado de obtener los registros de procedimiento en la base de datos
 * @param {Object} req
 * @param {Objetc} res
 */
const getProcedures = async (req, res) => {
    try {
        //Buscamos todos los datos
        const data = await ProceduresModules.findAll({
            where: {
                Estatus: '1'
            }
        });
        //Si no existen registros enviamos error
        if (!data) {
            handleHttpError(res, 'Los datos no existen.', 404);
            return
        }
        //Si existen datos los enviamos al front
        handleHttpSuccess(res, true, 'Se ha encontrado los datos.', 201, data)
    } catch (error) {
        //Obtener y devolver el error
        handleHttpError(res, 'Error obteniendo datos.', 403)
    }
};
/**
* Este controlador es el encargado de actualizar el registro
* @param {*} req 
* @param {*} res 
*/
const updateProcedure = async (req, res) => {
    try {
        //Obtenemos el procedimiento enviado por el cuerpo
        const { Id, ...body } = matchedData(req);
        //Obtenemos el cuerpo con la información que sera actualizado
        data = await ProceduresModules.findOne({
            where: {
                Id: Id
            }
        }).then(data => {
            if (!data) {
                handleHttpError(res, 'No se han encontrado datos', 403)
                return
            }
            //Actualizamos la información
            data.update(body)
            //Enviamos la respuesta de success
            handleHttpSuccess(res, true, 'Los datos han sido actualizados.', 201, data)
        })
    } catch (error) {
        console.log('error', error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error actualizando el procedimiento.', 403)
    }
}
/**
* Este controlador es el encargado de actualizar el registro
* @param {*} req 
* @param {*} res 
*/
const deleteProcedure = async (req, res) => {
    try {
        //Obtenemos el procedimiento enviado por el cuerpo
        const { Id } = matchedData(req);
        //Obtenemos el cuerpo con la información que sera actualizado
        data = await ProceduresModules.findOne({
            where: {
                Id: Id
            }
        }).then(data => {
            if (!data) {
                handleHttpError(res, 'No se han encontrado datos', 403)
                return
            }
            //Actualizamos la información
            data.update({Estatus: 0})
            //Enviamos la respuesta de success
            handleHttpSuccess(res, true, 'Los datos han sido actualizados.', 201, data)
        })
    } catch (error) {
        console.log('error', error)
        //Obtener y devolver el error
        handleHttpError(res, 'Error actualizando el procedimiento.', 403)
    }
}

module.exports = { createProcedure, getProcedures, updateProcedure, deleteProcedure };