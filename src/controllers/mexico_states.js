const MexicoStatesModules = require("../models/mexico_states");

const { handleHttpError } = require("../utils/handleError");

const { handleHttpSuccess } = require("../utils/handleSuccess");

const getMexicoStates = async (req, res) => {
  try {
    // Obtén todos los registros de la tabla
    const mexicoStates = await MexicoStatesModules.findAll();

    // Envía los registros al front-end
    handleHttpSuccess(
      res,
      true,
      "Se han encontrado los datos de los estados de México.",
      200,
      mexicoStates
    );
  } catch (error) {
    console.log("Error: ", error);
    // Devuelve el error al front-end
    handleHttpError(res, "Error obteniendo estados de México.", 500);
  }
};

module.exports = {
  getMexicoStates,
};
