const swaggerJsdoc = require("swagger-jsdoc");

//API Config Info
const swaggerDefinition = {
    openapi: "3.0.3",
    info: {
        title: "Documentación de api Metrica",
        version: "1.0.0"
    },
    servers: [
        {
            url: "http://localhost:3020/api"
        }
    ],
    components: {
        securitySchemes: {
            bearerAuth: {
                type: "http",
                scheme: "bearer",
                bearerFormat: "JWT",
            },
            ApiKeyAuth: {
                type: "apiKey",
                name: "api_key",
                in: "header"
            }
        },
        schemas: {
            userRegistration: {
                type: "object",
                required: ["Nombre", "ApellidoPaterno", "Email", "Contrasena", "Nickname", "Estatus"],
                properties: {
                    Nombre: {
                        type: "string"
                    },
                    ApellidoPaterno: {
                        type: "string"
                    },
                    ApellidoMaterno: {
                        type: "string"
                    },
                    Email: {
                        type: "string"
                    },
                    Contrasena: {
                        type: "string"
                    },
                    Nickname: {
                        type: "string"
                    },
                    Imagen: {
                        type: "string"
                    },
                    Estatus: {
                        type: "integer"
                    },
                }
            },
            authLogin: {
                type: "object",
                required: ["user", "Contrasena"],
                properties: {
                    user: {
                        type: "string",
                    },
                    Contrasena: {
                        type: "string",
                    },
                },
            },
            resetPassword: {
                type: "object",
                required: ["user"],
                properties: {
                    user: {
                        type: "string",
                    },
                },
            },
            getUsers: {
                type: "object",
            },
            deactivateUser: {
                type: "object",
                required: ["Guid"],
                properties: {
                    Guid: {
                        type: "string",
                    },
                },
            },
            activateUser: {
                type: "object",
                required: ["Guid"],
                properties: {
                    Guid: {
                        type: "string",
                    },
                },
            },
            getUser: {
                type: "object",
                required: ["Id", "Guid", "GuidRol", "Nombre", "ApellidoPaterno", "Email", "Contrasena", "Nickname", "UltimoLogin", "Imagen", "Estatus"],
                properties: {
                    Id: {
                        type: "integer"
                    },
                    Guid: {
                        type: "string"
                    },
                    GuidRol: {
                        type: "string"
                    },
                    Nombre: {
                        type: "string"
                    },
                    ApellidoPaterno: {
                        type: "string"
                    },
                    ApellidoMaterno: {
                        type: "string"
                    },
                    Email: {
                        type: "string"
                    },
                    Contrasena: {
                        type: "string"
                    },
                    Nickname: {
                        type: "string"
                    },
                    UltimoLogin: {
                        type: "date"
                    },
                    Imagen: {
                        type: "string"
                    },
                    Estatus: {
                        type: "integer"
                    },
                }
            },
            updatingUser: {
                type: "object",
                required: ["Id", "Guid", "Nombre", "ApellidoPaterno", "Email", "Nickname"],
                properties: {
                    Id: {
                        type: "integer"
                    },
                    Guid: {
                        type: "string"
                    },
                    Nombre: {
                        type: "string"
                    },
                    ApellidoPaterno: {
                        type: "string"
                    },
                    ApellidoMaterno: {
                        type: "string"
                    },
                    Email: {
                        type: "string"
                    },
                    Contrasena: {
                        type: "string"
                    },
                    Nickname: {
                        type: "string"
                    },
                    Imagen: {
                        type: "string"
                    }
                }
            },
            deleteUser: {
                type: "object",
                required: ["Guid"],
                properties: {
                    Guid: {
                        type: "string",
                    },
                },
            },
            getRoles: {
                type: "object",
            },
        }
    }
};
//Opciones
const options = {
    swaggerDefinition,
    security: [{ bearerAuth: [] }, { ApiKeyAuth: [] }],
    apis: [
        "./routes/*.js"
    ]
};

const openApiConfiguration = swaggerJsdoc(options)

module.exports = openApiConfiguration;