# Banco de Vibraciones - Backend

Bienvenido al proyecto del backend del Banco de Vibraciones, una API en tiempo real para monitorear y visualizar datos de sensores de Arduino.

## Descripción del Proyecto

Este proyecto está diseñado para recopilar datos de sensores de vibración conectados a un Arduino y proporcionar una API para visualizar esta información en tiempo real. La API permite a los usuarios acceder a los datos de vibración, configurar alertas y realizar un seguimiento del estado de los sensores en tiempo real.

## Características Principales

- Visualización en tiempo real de datos de sensores de vibración de Arduino.
- Conexión directa con Arduino para recopilar y procesar datos.
- Almacenamiento de datos en una base de datos PostgreSQL.
- Integración con Slack para notificaciones instantáneas.
- Generación de informes en formato Excel para análisis detallados.
- Validación de datos con Express Validator y Joi.
- Seguridad reforzada con encriptación mediante bcryptjs y JSON Web Tokens (JWT).

## Tecnologías utilizadas

- Node.js
- Express.js
- PostgreSQL
- Socket.IO
- Sequelize
- Swagger (swagger-jsdoc, swagger-ui-express)
- Redis
- Entre otras bibliotecas y paquetes detallados en el archivo package.json.

## Requisitos Previos

Antes de comenzar, asegúrate de tener instalado lo siguiente:

- Node.js y npm
- Arduino y sensores de vibración
- Dependencias del proyecto (se pueden instalar ejecutando `npm install`)

## Configuración

1. Clone este repositorio en su máquina local.
2. Ejecute `npm install` para instalar las dependencias del proyecto.
3. Configure la conexión con su Arduino y sensores en el archivo de configuración.

## Uso

1. Ejecute la aplicación utilizando `npm start` o `npm run dev` en modo de desarrollo.
2. Acceda a la API a través de la URL proporcionada en su navegador o aplicación cliente.

## Documentación

Para obtener más información sobre cómo utilizar la API y las rutas disponibles, consulte la documentación en [ENLACE A LA DOCUMENTACIÓN].

## Contribución

Si deseas contribuir a este proyecto, ¡te damos la bienvenida! Por favor, sigue estas pautas para contribuir.

## Autor

- Alex San German

## Licencia
Este proyecto está licenciado bajo la Licencia ISC. Consulta el archivo LICENSE para obtener más información.

---

Si tienes alguna pregunta o necesitas ayuda, no dudes en ponerte en contacto con nosotros.

¡Disfruta de tu proyecto de Banco de Vibraciones!
